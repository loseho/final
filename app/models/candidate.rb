class Candidate < ApplicationRecord
  validates :name,  presence: true, length: { maximum: 50 }
  validates :email, presence: true, length: { maximum: 255 }
  validates :phone, presence: true
  validates :address, presence: true, length: { maximum: 255 }
  validates :position, presence: true
  validates :pre_salary, presence: true
  validates :expect_salary, presence: true
  validates :source, presence: true
  validates :source_detail, presence: true
end
