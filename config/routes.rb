Rails.application.routes.draw do
  get 'test_page/index'

  resources :candidates

  # get  '/candidate',    to: 'candidates#index'
  # get  '/new',  to: 'candidates#new'
  # get  '/edit',    to: 'candidates#edit'
  # get  '/delete',    to: 'candidates#delete'
  # post '/create',  to: 'candidates#create'
  # root 'candidates#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'candidates#index'
end
