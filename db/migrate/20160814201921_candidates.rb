class Candidates < ActiveRecord::Migration[5.0]
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.string :address
      t.string :position
      t.float :pre_salary
      t.float :expect_salary
      t.string :source
      t.string :source_detail

      t.timestamps
    end
  end
end
